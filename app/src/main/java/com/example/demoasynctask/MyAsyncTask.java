package com.example.demoasynctask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class MyAsyncTask extends AsyncTask<String, Integer, String> {
    Activity contextParent;
    private ProgressDialog prgDialog;
    private MediaPlayer mPlayer;
    public static final int progress_bar_type = 0;
    public MyAsyncTask(Activity contextParent) {
        this.contextParent = contextParent;
    }


//
//    @Override
//    protected void onPreExecute() {
//        super.onPreExecute();
//        Toast.makeText(contextParent, "Start Loading", Toast.LENGTH_SHORT).show();
//    }
//
////    @Override
////    protected Void doInBackground(Void... voids) {
////        for (int i = 0; i <= 100; i++) {
////            SystemClock.sleep(100);
////            publishProgress(i);
////        }
////        return null;
////    }
//    @Override
//    protected String doInBackground(String... url) {
//        for (int i = 0; i <= 100; i++) {
//            SystemClock.sleep(100);
//            publishProgress(i);
//        }
//        return "dfsd";
//    }
//
//    @SuppressLint("SetTextI18n")
//    @Override
//    protected void onProgressUpdate(Integer... values) {
//        super.onProgressUpdate(values);
//        ProgressBar progressBar = contextParent.findViewById(R.id.prbDemo);
////        vì publishProgress chỉ truyền 1 đối số nên mảng values chỉ có 1 phần tử
//        progressBar.setProgress(values[0]);
//        TextView text = contextParent.findViewById(R.id.txtStatus);
//        text.setText(values[0] + "%");
//    }
//
//    @Override
//    protected void onPostExecute(String aVoid) {
//        super.onPostExecute(aVoid);
//        Toast.makeText(contextParent, "Finished", Toast.LENGTH_LONG).show();
//    }

    // Show Progress bar before downloading Music
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // Shows Progress Bar Dialog and then call doInBackground method
        contextParent.showDialog(progress_bar_type);
    }

    // Download Music File from Internet
    @Override
    protected String doInBackground(String... f_url) {
        int count;
        try {
            URL url = new URL(f_url[0]);
            URLConnection conection = url.openConnection();
            conection.connect();
            // Get Music file length
            int lenghtOfFile = conection.getContentLength();
            // input stream to read file - with 8k buffer
            InputStream input = new BufferedInputStream(url.openStream(),10*1024);
            // Output stream to write file in SD card
            OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().getPath()+"/jai_ho.mp3");
            byte data[] = new byte[1024];
            long total = 0;
            while ((count = input.read(data)) != -1) {
                total += count;
                // Publish the progress which triggers onProgressUpdate method
                publishProgress(Integer.valueOf("" + (int) ((total * 100) / lenghtOfFile)));

                // Write data to file
                output.write(data, 0, count);
            }
            // Flush output
            output.flush();
            // Close streams
            output.close();
            input.close();
        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }
        return null;
    }

    // While Downloading Music File
    protected void onProgressUpdate(String... progress) {
        // Set progress percentage
        prgDialog.setProgress(Integer.parseInt(progress[0]));
    }

    // Once Music File is downloaded
    @Override
    protected void onPostExecute(String file_url) {
        // Dismiss the dialog after the Music file was downloaded
        contextParent.dismissDialog(progress_bar_type);
        Toast.makeText(contextParent.getApplicationContext(), "Download complete, playing Music", Toast.LENGTH_LONG).show();
        // Play the music
        playMusic();
    }

    public void playMusic(){
        // Read Mp3 file present under SD card
        Uri myUri1 = Uri.parse("file:///sdcard/jai_ho.mp3");
        mPlayer  = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mPlayer.setDataSource(contextParent, myUri1);
            mPlayer.prepare();
            // Start playing the Music file
            mPlayer.start();
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    // TODO Auto-generated method stub
                    // Once Music is completed playing, enable the button

                    Toast.makeText(contextParent.getApplicationContext(), "Music completed playing", Toast.LENGTH_LONG).show();
                }
            });
        } catch (IllegalArgumentException e) {
            Toast.makeText(contextParent.getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (SecurityException e) {
            Toast.makeText(contextParent.getApplicationContext(), "URI cannot be accessed, permissed needed", Toast.LENGTH_LONG).show();
        } catch (IllegalStateException e) {
            Toast.makeText(contextParent.getApplicationContext(), "Media Player is not in correct state", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(contextParent.getApplicationContext(), "IO Error occured", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(contextParent.getApplicationContext(), "Exception Error occured", Toast.LENGTH_LONG).show();
        }
    }
}
